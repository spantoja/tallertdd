__author__ = 'servio pantoja'

class ProcesadorNumeros:
    def procesarNumeros(self,cadena):
        retorno = []
        cantidad=0
        minimo=0
        maximo=0
        promedio=0
        if "," in cadena or ":" in cadena or "&" in cadena:
            cadena = cadena.replace(":",",")
            cadena = cadena.replace("&", ",")
            numeros = map(int,cadena.split(","))
            promedio = sum(numeros)/len(numeros)
            cantidad = len(numeros)
            minimo=min(numeros)
            maximo=max(numeros)
        elif cadena!="":
            promedio=int(cadena)
            cantidad=len(cadena)
            minimo=int(cadena)
            maximo =int(cadena)
        retorno.append(cantidad)
        retorno.append(minimo)
        retorno.append(maximo)
        retorno.append(promedio)
        return retorno



