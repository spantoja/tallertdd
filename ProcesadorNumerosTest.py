from unittest import TestCase

from ProcesadorNumeros import ProcesadorNumeros


class ProcesadorNumerosTest(TestCase):
    def test_cadena_vacia_contar_numero_elementos(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("")[0], 0,
                         "Cadena vacia y array con primer numero cantidad de numeros")

    def test_cadena_un_caracter_contar_numero_elementos(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("3")[0], 1,
                         "Cadena un digito y array con primer numero cantidad de numeros")

    def test_cadena_dos_caracteres_contar_numero_elementos(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("3,3")[0], 2,
                         "Cadena dos digitos y array con primer numero cantidad de numeros")

    def test_cadena_muchos_caracteres_contar_numero_elementos(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1,6,5,7,2")[0], 5,
                         "Cadena n digitos y array con primer numero cantidad de numeros")

    def test_cadena_vacia_cantidad_y_minimo(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("")[0], 0,
                         "Cadena vacia, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("")[1], 0,
                         "Cadena vacia, minimo")

    def test_cadena_un_caracter_cantidad_y_minimo(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1")[0], 1,
                         "Cadena un caracter, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("4")[1], 4,
                         "Cadena un caracter, minimo")

    def test_cadena_dos_caracteres_cantidad_y_minimo(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1,2")[0], 2,
                         "Cadena dos caracteres, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("5,3")[1], 3,
                         "Cadena dos caracteres, minimo")

    def test_cadena_n_caracteres_cantidad_y_minimo(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1,2,4,4")[0], 4,
                         "Cadena n caracteres, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1,2,3")[1], 1,
                         "Cadena n caracteres, minimo")

    def test_vacia_cantidad_y_minimo_y_maximo(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("")[0], 0,
                         "Cadena vacia, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("")[1], 0,
                         "Cadena vacia, minimo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("")[2], 0,
                         "Cadena vacia, maximo")

    def test_un_numero_cantidad_y_minimo_y_maximo(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1")[0], 1,
                         "Cadena un numero, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("2")[1], 2,
                         "Cadena un numero, minimo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("3")[2], 3,
                         "Cadena un numero, maximo")

    def test_dos_numeros_cantidad_y_minimo_y_maximo(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1,2")[0], 2,
                         "Cadena dos numeros, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("3,4")[1], 3,
                         "Cadena dos numeros, minimo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("5,6")[2], 6,
                         "Cadena dos numeros, maximo")

    def test_n_numeros_cantidad_y_minimo_y_maximo(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1,2,3")[0], 3,
                         "Cadena n numeros, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("4,5,6")[1], 4,
                         "Cadena n numeros, minimo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("7,8,9")[2], 9,
                         "Cadena n numeros, maximo")

    def test_vacia_cantidad_y_minimo_y_maximo_y_promedio(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("")[0], 0,
                         "Cadena vacia, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("")[1], 0,
                         "Cadena vacia, minimo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("")[2], 0,
                         "Cadena vacia, maximo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("")[3], 0,
                         "Cadena vacia, promedio")

    def test_un_numero_cantidad_y_minimo_y_maximo_y_promedio(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1")[0], 1,
                         "Cadena un numero, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("2")[1], 2,
                         "Cadena un numero, minimo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("3")[2], 3,
                         "Cadena un numero, maximo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("4")[3], 4,
                         "Cadena un numero, promedio")

    def test_dos_numeros_cantidad_y_minimo_y_maximo_y_promedio(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1,2")[0], 2,
                         "Cadena dos numeros, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("2,3")[1], 2,
                         "Cadena dos numeros, minimo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("3,4")[2], 4,
                         "Cadena dos numeros, maximo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("5,5")[3], 5,
                         "Cadena dos numeros, promedio")

    def test_n_numeros_cantidad_y_minimo_y_maximo_y_promedio(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1,2,3")[0], 3,
                         "Cadena n numeros, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("2,3,4")[1], 2,
                         "Cadena n numeros, minimo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("3,4,5")[2], 5,
                         "Cadena n numeros, maximo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("6,4,5")[3], 5,
                         "Cadena n numeros, promedio")

    def test_n_numeros_y_caracteres_especiales_cantidad_y_minimo_y_maximo_y_promedio(self):
        self.assertEqual(ProcesadorNumeros().procesarNumeros("1:2&3")[0], 3,
                         "Cadena n numeros y caracter especial, cantidad")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("2,3&4")[1], 2,
                         "Cadena n numeros y caracter especial, minimo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("3:4,5")[2], 5,
                         "Cadena n numeros y caracter especial, maximo")
        self.assertEqual(ProcesadorNumeros().procesarNumeros("6:4:5")[3], 5,
                         "Cadena n numeros y caracter especial, promedio")